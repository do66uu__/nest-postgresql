import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
} from "@nestjs/common";
import { CreatePostDto } from "./dto/create-post.dto";
import { PostService } from "./posts.service";
import { FileInterceptor } from "@nestjs/platform-express";
import { UpdatePostDto } from "./dto/update-post.dto";

@Controller("posts")
export class PostController {
  constructor(private postService: PostService) {}

  @Post()
  @UseInterceptors(FileInterceptor("image"))
  create(@Body() postDto: CreatePostDto, @UploadedFile() image) {
    return this.postService.create(postDto, image);
  }

  @Get()
  async getAllPosts() {
    return await this.postService.findAll();
  }

  @Patch(":id")
  async update(@Param("id") id: string, @Body() updatePostDto: UpdatePostDto) {
    return await this.postService.update(id, updatePostDto);
  }

  @Get("/:id")
  async getPost(@Param("id") id: string) {
    return await await this.postService.findOne(id);
  }

  @Delete(":id")
  async remove(@Param("id") id: string) {
    return await this.postService.remove(id);
  }
}
