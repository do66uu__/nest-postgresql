import { Posts, Prisma, TechicType } from "@prisma/client";

export class PostEntity implements Posts {
  id: string;
  type: TechicType;
  name: string;
  description: string | null;
  image: string | null;
  brand: string;
  model: string;
  year: string;
  cost: Prisma.Decimal;
  isPublished: boolean;
  lastPublished: Date;
  viewCount: Prisma.Decimal;
  company: string;
  createdAt: Date;
  updatedAt: Date;
}
