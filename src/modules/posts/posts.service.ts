import { Posts } from "@prisma/client";
import { Injectable } from "@nestjs/common";
import { CreatePostDto } from "./dto/create-post.dto";
import { UpdatePostDto } from "./dto/update-post.dto";
import { FilesService } from "src/core/files/files.service";
import { PrismaService } from "src/core/prisma/prisma.service";

@Injectable()
export class PostService {
  constructor(
    private prisma: PrismaService,
    private fileService: FilesService
  ) {}

  async create(createPostDto: CreatePostDto, image: any): Promise<Posts> {
    try {
      let fileName;
      if (image) {
        fileName = await this.fileService.createFile(image);
      }
      const dataForCreatePost: CreatePostDto = {
        ...createPostDto,
        image: fileName,
      };

      const post = await this.prisma.posts.create({
        data: dataForCreatePost,
      });
      return post;
    } catch (err) {
      throw err;
    }
  }

  async findAll(): Promise<Posts[]> {
    return await this.prisma.posts.findMany();
  }

  async findOne(id: string) {
    return await this.prisma.posts.findUnique({ where: { id } });
  }

  async update(id: string, updatePostDto: UpdatePostDto): Promise<Posts> {
    return await this.prisma.posts.update({
      where: { id },
      data: updatePostDto,
    });
  }

  async remove(id: string): Promise<Posts> {
    return await this.prisma.posts.delete({ where: { id } });
  }
}
