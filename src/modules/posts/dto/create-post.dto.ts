import { TechicType } from "@prisma/client";

export class CreatePostDto {
  readonly name: string;
  readonly model: string;
  readonly cost: number;
  readonly type: TechicType;
  readonly brand: string;
  readonly year: string;
  readonly lastPublished: Date;
  readonly company: string;
  readonly image?: string;
  readonly description?: string;
  readonly viewCount?: number;
  readonly isPublished: boolean = false;
}
