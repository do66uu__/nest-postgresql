import { Module } from "@nestjs/common";
import { FilesModule } from "src/core/files/files.module";
import { UsersModule } from "../users/users.module";
import { PostController } from "./posts.controller";
import { PostService } from "./posts.service";

@Module({
  controllers: [PostController],
  providers: [PostService],
  imports: [UsersModule, FilesModule],
})
export class PostModule {}
