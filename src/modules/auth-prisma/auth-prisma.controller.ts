import { Body, Controller, Post } from "@nestjs/common";
import { AuthPrismaService } from "./auth-prisma.service";
import { LoginDto } from "./dto/login.dto";
import { SignUpDto } from "./dto/signup.dto";

@Controller("auth")
export class AuthPrismaController {
  constructor(private readonly authPrismaService: AuthPrismaService) {}

  @Post("login")
  login(@Body() { email, password }: LoginDto) {
    return this.authPrismaService.login(email, password);
  }

  @Post("/signup")
  registration(@Body() userDto: SignUpDto) {
    return this.authPrismaService.registration(userDto);
  }
}
