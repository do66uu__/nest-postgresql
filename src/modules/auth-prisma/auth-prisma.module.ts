import { Module } from "@nestjs/common";
import { AuthPrismaService } from "./auth-prisma.service";
import { AuthPrismaController } from "./auth-prisma.controller";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { JwtStrategy } from "./jwt.strategy";
import { MailModule } from "src/core/mail/mail.module";

// don't expose your keys, move them into .env file
export const jwtSecret = "prismaDay2021";

@Module({
  imports: [
    PassportModule,
    MailModule,
    JwtModule.register({
      secret: jwtSecret,
      signOptions: { expiresIn: "60s" },
    }),
  ],
  controllers: [AuthPrismaController],
  providers: [AuthPrismaService, JwtStrategy],
})
export class AuthPrismaModule {}
