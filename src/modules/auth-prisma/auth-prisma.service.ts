import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from "bcryptjs";
import { MailService } from "src/core/mail/mail.service";
import { PrismaService } from "src/core/prisma/prisma.service";
import { SignUpDto } from "./dto/signup.dto";
import { Auth } from "./entity/auth.entity";

@Injectable()
export class AuthPrismaService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private mailService: MailService
  ) {}

  async login(email: string, password: string): Promise<Auth> {
    const user = await this.prisma.user.findUnique({ where: { email } });

    if (!user) {
      throw new NotFoundException(`No user found for email: ${email}`);
    }

    const passwordValid = await bcrypt.compare(password, user.password);

    if (!passwordValid) {
      throw new UnauthorizedException("Invalid password");
    }

    return {
      accessToken: this.jwtService.sign({ userId: user.id }),
    };
  }

  async registration(userDto: SignUpDto): Promise<Auth> {
    if (userDto.password !== userDto.confirmPassword) {
      throw new UnauthorizedException(`Password doesn't equal`);
    }

    const candidate = await this.prisma.user.findUnique({
      where: { email: userDto.email },
    });

    if (candidate) {
      throw new UnauthorizedException(`User Exist`);
    }

    const hashPassword = await bcrypt.hash(userDto.password, 5);

    const { confirmPassword, ...user_data } = userDto;
    user_data.password = hashPassword;
    console.log("confirmPassword:", confirmPassword);

    const user = await this.prisma.user.create({
      data: user_data,
    });

    const accessToken = await this.jwtService.sign({ userId: user.id });
    await this.mailService.sendUserConfirmation(user, accessToken);

    return { accessToken };
  }

  async validateUser(userId: string) {
    return await this.prisma.user.findUnique({ where: { id: userId } });
  }
}
