import { IsEmail, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class SignUpDto {
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  readonly password: string;

  @IsNotEmpty()
  readonly confirmPassword: string;

  @IsString()
  @IsOptional()
  readonly userName?: string;

  @IsString()
  readonly name: string;

  @IsString()
  @IsOptional()
  readonly surname?: string;
}
