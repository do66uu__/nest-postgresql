import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from "@nestjs/common";
import { RolesService } from "./roles.service";
import { CreateRoleDto } from "./dto/create-role.dto";
import { UpdateRoleDto } from "./dto/update-role.dto";

@Controller("roles")
export class RolesController {
  constructor(private roleService: RolesService) {}

  @Post()
  async create(@Body() dto: CreateRoleDto) {
    return await this.roleService.create(dto);
  }

  @Get()
  async findAll() {
    return await this.roleService.findAll();
  }

  @Get("/:value")
  async getByValue(@Param("value") value: string) {
    return await this.roleService.getRoleByValue(value);
  }

  @Patch(":id")
  async update(@Param("id") id: string, @Body() updateRoleDto: UpdateRoleDto) {
    return await this.roleService.update(id, updateRoleDto);
  }

  @Delete(":id")
  async remove(@Param("id") id: string) {
    return await this.roleService.remove(id);
  }
}
