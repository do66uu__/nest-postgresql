import { Injectable } from "@nestjs/common";
import { CreateRoleDto } from "./dto/create-role.dto";
import { Roles } from ".prisma/client";
import { UpdateRoleDto } from "./dto/update-role.dto";
import { PrismaService } from "src/core/prisma/prisma.service";

@Injectable()
export class RolesService {
  constructor(private prisma: PrismaService) {}

  async create(createRoleDto: CreateRoleDto): Promise<Roles> {
    const role = await this.prisma.roles.create({ data: createRoleDto });
    return role;
  }

  async getRoleByValue(value: string): Promise<Roles> {
    const role = await this.prisma.roles.findFirst({
      where: { value: value },
      include: {
        users: true,
        User_roles: true,
      },
    });
    return role;
  }

  async findAll(): Promise<Roles[]> {
    return await this.prisma.roles.findMany();
  }

  async update(id: string, updateRoleDto: UpdateRoleDto): Promise<Roles> {
    return await this.prisma.roles.update({
      where: { id },
      data: updateRoleDto,
    });
  }

  async remove(id: string): Promise<Roles> {
    return await this.prisma.roles.delete({ where: { id } });
  }
}
