import { User_roles } from "@prisma/client";

export class RoleUserEntity implements User_roles {
  roleId: string;
  userId: string;
  createdAt: Date;
  updatedAt: Date;
}
