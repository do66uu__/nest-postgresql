import { Roles } from "@prisma/client";

export class RoleEntity implements Roles {
  id: string;
  value: string;
  description: string | null;
  createdAt: Date;
  updatedAt: Date;
}
