import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
} from "@nestjs/common";
import { ProductsService } from "./products.service";
import { CreateProductDto } from "./dto/create-product.dto";
import { UpdateProductDto } from "./dto/update-product.dto";
import { ProductEntity } from "./entities/product.entity";
import { ConnectionArgs } from "../pages/dto/connection-args.dto";
import { JwtPrismaGuard } from "../auth-prisma/jwt-auth.guard";

@Controller("products")
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  async create(@Body() createProductDto: CreateProductDto) {
    return new ProductEntity(
      await this.productsService.create(createProductDto)
    );
  }

  @Get()
  async findAll() {
    const products = await this.productsService.findAll();
    return products.map((product) => new ProductEntity(product));
  }

  @Get("drafts")
  @UseGuards(JwtPrismaGuard) // 🔐
  async findDrafts() {
    const drafts = await this.productsService.findDrafts();
    return drafts.map((product) => new ProductEntity(product));
  }

  @Get("page")
  async findPage(@Query() connectionArgs: ConnectionArgs) {
    console.log("connectionArgsAAA:", connectionArgs);

    return this.productsService.findPage(connectionArgs);
  }

  @Get(":id")
  async findOne(@Param("id") id: string) {
    return new ProductEntity(await this.productsService.findOne(id));
  }

  @Patch(":id")
  async update(
    @Param("id") id: string,
    @Body() updateProductDto: UpdateProductDto
  ) {
    return new ProductEntity(
      await this.productsService.update(id, updateProductDto)
    );
  }

  @Delete(":id")
  async remove(@Param("id") id: string) {
    return new ProductEntity(await this.productsService.remove(id));
  }
}
