import { ProductEntity } from "./entities/product.entity";
import { ConnectionArgs } from "../pages/dto/connection-args.dto";
import { Injectable } from "@nestjs/common";
import { CreateProductDto } from "./dto/create-product.dto";
import { UpdateProductDto } from "./dto/update-product.dto";
import { findManyCursorConnection } from "@devoxa/prisma-relay-cursor-connection";
import { Prisma, Product } from "@prisma/client";
import { Page } from "../pages/dto/page.dto";
import { PrismaService } from "src/core/prisma/prisma.service";

@Injectable()
export class ProductsService {
  constructor(private prisma: PrismaService) {}

  async create(createProductDto: CreateProductDto): Promise<Product> {
    return await this.prisma.product.create({ data: createProductDto });
  }

  async findAll(): Promise<Product[]> {
    return await this.prisma.product.findMany({ where: { published: true } });
  }

  async findDrafts(): Promise<Product[]> {
    return await this.prisma.product.findMany({ where: { published: false } });
  }

  async findOne(id: string): Promise<Product> {
    return await this.prisma.product.findUnique({ where: { id: id } });
  }

  async update(
    id: string,
    updateProductDto: UpdateProductDto
  ): Promise<Product> {
    return await this.prisma.product.update({
      where: { id: id },
      data: updateProductDto,
    });
  }

  async remove(id: string): Promise<Product> {
    return await this.prisma.product.delete({ where: { id: id } });
  }

  async findPage(connectionArgs: ConnectionArgs) {
    const where: Prisma.ProductWhereInput = {
      published: true,
    };
    const productPage = await findManyCursorConnection(
      // 👇 args contain take, skip and cursor
      (args) =>
        this.prisma.product.findMany({
          ...args, // 👈 apply paging arguments
          where: where,
        }),
      () =>
        this.prisma.product.count({
          where: where,
        }),
      connectionArgs, // 👈 returns all product records
      {
        recordToEdge: (record) => ({
          node: new ProductEntity(record), // 👈 instance to transform price
        }),
      }
    );
    console.log("productPageAAA:", productPage);
    console.log("connectionArgsAAA:", connectionArgs);

    return new Page<ProductEntity>(productPage); // 👈 instance as this object is returned
  }
}
