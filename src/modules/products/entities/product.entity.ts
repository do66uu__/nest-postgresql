import { Product, Prisma } from "@prisma/client";
import { Transform } from "class-transformer";

export class ProductEntity implements Product {
  id: string;
  name: string;
  description: string | null;
  @Transform(({ value }) => value.toNumber())
  price: Prisma.Decimal;
  sku: string;
  published: boolean;
  createdAt: Date;
  updatedAt: Date;

  constructor(partial: Partial<ProductEntity>) {
    Object.assign(this, partial);

    // short for 👇
    // this.id = partial.id;
    // this.createdAt = partial.createdAt;
    // this.updatedAt = partial.updatedAt;
    // this.name = partial.name;
    // this.description = partial.description;
    // this.price = partial.price;
    // this.sku = partial.sku;
    // this.published = partial.published;
  }
}
