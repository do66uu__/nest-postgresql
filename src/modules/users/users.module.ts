import { forwardRef, Module } from "@nestjs/common";
import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";
import { RolesModule } from "../roles/roles.module";
import { AuthModule } from "../auth/auth.module";
import { MailModule } from "src/core/mail/mail.module";

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [RolesModule, forwardRef(() => AuthModule), MailModule],
  exports: [UsersService],
})
export class UsersModule {}
