import { User } from "@prisma/client";

export class UserEntity implements User {
  id: string;
  userName: string | null;
  name: string;
  surname: string | null;
  email: string;
  password: string;
  role: string;
  banned: boolean;
  isActive: boolean;
  banReason: string | null;
  avatarId: string | null;
  createdAt: Date;
  updatedAt: Date;
}
