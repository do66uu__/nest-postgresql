import { Injectable } from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { RolesService } from "../roles/roles.service";
import { User } from "@prisma/client";
import { UpdateUsertDto } from "./dto/update-user.dto";
import { PrismaService } from "src/core/prisma/prisma.service";
import { MailService } from "src/core/mail/mail.service";

@Injectable()
export class UsersService {
  constructor(
    private prisma: PrismaService,
    private roleService: RolesService,
    private mailService: MailService
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = await this.prisma.user.create({ data: createUserDto });
    const role = await this.roleService.getRoleByValue("ADMIN");

    await this.prisma.user_roles.create({
      data: { roleId: role.id, userId: user.id },
    });

    const token = Math.floor(1000 + Math.random() * 9000).toString();
    await this.mailService.sendUserConfirmation(user, token);

    return user;
  }

  async findAll(): Promise<User[]> {
    return await this.prisma.user.findMany({
      include: {
        roles: true,
        company: true,
      },
    });
  }

  async getUserByEmail(email: string): Promise<User> {
    const user = await this.prisma.user.findFirst({
      where: {
        email,
      },
      include: {
        roles: true,
        User_roles: true,
        company: true,
      },
    });
    return user;
  }

  async update(id: string, updateRoleDto: UpdateUsertDto): Promise<User> {
    return await this.prisma.user.update({
      where: { id },
      data: updateRoleDto,
    });
  }

  async remove(id: string): Promise<User> {
    return await this.prisma.user.delete({ where: { id } });
  }
}
