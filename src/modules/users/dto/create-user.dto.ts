export class CreateUserDto {
  readonly email: string;
  readonly password: string;
  readonly password_repeat: string;
  readonly userName?: string;
  readonly name: string;
  readonly surname?: string;
  readonly role?: string;
  readonly banned?: boolean;
  readonly banReason?: string;
  readonly avatarId?: string;
}
