export class CreateCompanyDto {
  readonly name: string;
  readonly address: string;
  readonly description: string;
  readonly phone: string;
  readonly site: string;
  readonly email: string;
  readonly legal: string;
  readonly rating: string;
  readonly logoId?: string;
  readonly userId: string;
}
