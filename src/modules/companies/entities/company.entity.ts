import { Company } from "@prisma/client";

export class CompanyEntity implements Company {
  id: string;
  name: string;
  address: string;
  description: string;
  phone: string;
  site: string;
  email: string;
  legal: string;
  rating: string;
  logoId: string | null;
  userId: string;
  createdAt: Date;
  updatedAt: Date;
}
