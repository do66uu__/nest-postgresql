import { Injectable } from "@nestjs/common";
import { Company } from "@prisma/client";
import { PrismaService } from "src/core/prisma/prisma.service";
import { CreateCompanyDto } from "./dto/create-company.dto";
import { UpdateCompanyDto } from "./dto/update-company.dto";

@Injectable()
export class CompaniesService {
  constructor(private prisma: PrismaService) {}

  async create(createCompanyDto: CreateCompanyDto): Promise<Company> {
    const role = await this.prisma.company.create({ data: createCompanyDto });
    return role;
  }

  async findAll(): Promise<Company[]> {
    return await this.prisma.company.findMany();
  }

  async findOne(id: string): Promise<Company> {
    return await this.prisma.company.findUnique({ where: { id } });
  }

  async update(
    id: string,
    updateCompanyDto: UpdateCompanyDto
  ): Promise<Company> {
    return await this.prisma.company.update({
      where: { id },
      data: updateCompanyDto,
    });
  }

  async remove(id: string): Promise<Company> {
    return await this.prisma.company.delete({ where: { id } });
  }
}
