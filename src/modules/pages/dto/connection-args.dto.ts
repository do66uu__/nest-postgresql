import { IsNumber } from "class-validator";

export class ConnectionArgs {
  @IsNumber()
  first?: number;

  @IsNumber()
  last?: number;
  after?: string;
  before?: string;
}
