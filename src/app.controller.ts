import { Controller, Get } from "@nestjs/common";
import { PrismaService } from "./core/prisma/prisma.service";

@Controller()
export class AppController {
  constructor(private prisma: PrismaService) {}

  @Get("products")
  findProducts() {
    return this.prisma.product.findMany({
      where: { published: true },
    });
  }
}
