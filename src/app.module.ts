import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { ServeStaticModule } from "@nestjs/serve-static";
import { AppController } from "./app.controller";
import * as path from "path";
import { UsersModule } from "./modules/users/users.module";
import { AuthModule } from "./modules/auth/auth.module";
import { RolesModule } from "./modules/roles/roles.module";
import { PostModule } from "./modules/posts/posts.module";
import { FilesModule } from "./core/files/files.module";
import { PrismaModule } from "./core/prisma/prisma.module";
import { ProductsModule } from "./modules/products/products.module";
import { AuthPrismaModule } from "./modules/auth-prisma/auth-prisma.module";
import { ConfigModule } from "@nestjs/config";
import { MailModule } from "./core/mail/mail.module";
import { CompaniesModule } from "./modules/companies/companies.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `environment/${process.env.NODE_ENV}.env`,
      isGlobal: true, // no need to import into other modules
    }),
    ServeStaticModule.forRoot({
      rootPath: path.resolve(__dirname, "static"),
      serveRoot: "/file",
    }),
    SequelizeModule.forRoot({
      dialect: "postgres",
      host: process.env.POSTGRES_HOST,
      port: Number(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      autoLoadModels: true,
    }),
    UsersModule,
    RolesModule,
    AuthModule,
    PostModule,
    FilesModule,
    PrismaModule,
    ProductsModule,
    AuthPrismaModule,
    MailModule,
    CompaniesModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
