import * as faker from "faker";

export const createDevice = () => ({
  id: "q1222",
  name: faker.company.companyName(),
  description: faker.company.catchPhraseDescriptor(),
  brand: faker.company.companyName(),
  model: faker.vehicle.model(),
  type: faker.vehicle.type(),
  year: faker.date.past(),
  cost: `${faker.datatype.number()}`,
  isPublished: faker.helpers.randomize([true, false]),
  lastPublished: faker.date.past(),
  companyId: `1`,
});
