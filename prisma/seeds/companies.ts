import * as faker from "faker";

const belCities = [
  { id: "Брестская обл.", value: "Брестская обл." },
  { id: "Витебская обл.", value: "Витебская обл." },
  { id: "Гомельская обл.", value: "Гомельская обл." },
  { id: "Гродненская обл.", value: "Гродненская обл." },
  { id: "Минская обл.", value: "Минская обл." },
  { id: "Могилевская обл.", value: "Могилевская обл." },
];

const randomItem = <T>(arr: T[]): T => {
  return arr[Math.floor(Math.random() * arr.length)];
};

export const getImage = () => ({
  path: faker.image.transport(160, 160) + `?q=${faker.datatype.number()}`,
  name: faker.name.jobTitle(),
  size: faker.datatype.number(),
  mimetype: "",
  destination: "local",
});

export const getViewCount = () => ({
  value: faker.datatype.number(10000),
});

export const createCompany = () => ({
  id: "",
  name: faker.company.companyName(),
  address: randomItem(belCities).id,
  description: faker.company.catchPhraseDescriptor(),
  phone: faker.phone.phoneNumber(),
  site: faker.internet.domainName(),
  email: faker.internet.email(),
  legal: faker.address.countryCode(),
  rating: `2 + ${faker.datatype.number(3)}`,
  userId: "1",
});
