import * as faker from "faker";

export const createUser = () => ({
  id: "111",
  userName: faker.internet.userName(),
  name: faker.name.findName(),
  surname: faker.name.lastName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
});
