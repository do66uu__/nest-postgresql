import { PrismaClient } from "@prisma/client";
import { createCompany } from "./seeds/companies";
import { createDevice } from "./seeds/devices";
import { createUser } from "./seeds/users";

const prisma = new PrismaClient({
  log: ["query", "info", "warn", "error"],
});

async function main() {
  for (let i = 1; i <= 10; i++) {
    const deviceSeed = createDevice();
    const userSeed = createUser();
    const companySeed = createCompany();

    userSeed.id = `${i}`;
    companySeed.id = `${i}`;
    companySeed.userId = `${i}`;
    deviceSeed.id = `${i}`;
    deviceSeed.companyId = `${i}`;

    await prisma.user.create({ data: userSeed });
    await prisma.company.create({ data: companySeed });
    await prisma.device.create({ data: deviceSeed as any });
  }
  // for (const product of products) {
  // await prisma.product.create({ data: product });
  // }
}

main()
  .catch((e) => {
    console.log(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
