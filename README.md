# Backend Project

Проект основан на **Nest.js**

База данных **PostgreSQL**

Как запустить проект через yarn

1. yarn (при первом запуске)
2. yarn build:dev / yarn build:prod (при первом запуске)
3. yarn start:docker
4. docker exec -it local_db_kol psql -U postgres -c "CREATE DATABASE kolhos_postgres;" (при первом запуске)
5. yarn start или yarn start:dev
6. yarn db:migrate

Как запустить проект через npm

1. npm i (при первом запуске)
2. npm run build:dev / npm run build:prod (при первом запуске)
3. npm run start:docker
4. docker exec -it local_db_kol psql -U postgres -c "CREATE DATABASE kolhos_postgres;" (при первом запуске)
5. npm run start или npm run start:dev
6. npm run start или npm run start:dev
